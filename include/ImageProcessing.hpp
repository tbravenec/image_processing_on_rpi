#include <opencv4/opencv2/opencv.hpp>

#ifndef IMAGEPROCESSING_HPP
#define IMAGEPROCESSING_HPP

namespace ImageProcessing
{    
    cv::Mat threshold(cv::Mat image, unsigned char thresh, unsigned char max, int threads);
    cv::Mat edgeDetection(cv::Mat image, unsigned char threshold, int threads);

    class canny
    {
        private:
            cv::Mat image;
            cv::Mat gaussian;
            cv::Mat angle;
            cv::Mat sobel;
            cv::Mat nonMax;
            cv::Mat output;

            unsigned char thresh1;
            unsigned char thresh2;
            int threadCount;

            cv::Mat gaussianBlur();
            cv::Mat sobelAndAngleMap();
            cv::Mat nonMaximumSuppresion();
            cv::Mat cannyThreshold();
        public:
            cv::Mat run();
            canny(cv::Mat gray, unsigned char threshold1, int threads);
            ~canny();
    };

    class Cluster
    {
        public:
            void randomizeCentroid();
            void setCentroid(unsigned char blue, unsigned char green, unsigned char red);
            void recalculateCentroids();
            void clearValues();
            void addRedValue(unsigned char value) { sumRed += value; }
            void addBlueValue(unsigned char value) { sumBlue += value; }
            void addGreenValue(unsigned char value) { sumGreen += value; }
            void increaseCount() { count++; }
        
            // Center values of a cluster
            unsigned char centroidRed;
            unsigned char centroidBlue;
            unsigned char centroidGreen;

        private:
            // Maximum number of sum of pixel colors
            long sumRed;
            long sumBlue;
            long sumGreen;

            // Maximum number of subpixels in image
            int count;
    };

    class KMeans
    {
        private:
            cv::Mat image;
            cv::Mat clustered;

            Cluster *newCluster;
            Cluster *oldCluster;

            int clusterCount;
            int maxIterations;
            int threadCount;

            void iterate();
            void newToOld();
            bool checkConvergence();
            void recalculateCentroids();
            
        public:
            cv::Mat run();
            KMeans(cv::Mat rgb, int k, int iterations, int threads);
            ~KMeans();
    };    
};

#endif