#include <opencv4/opencv2/opencv.hpp>

#ifndef UTILS_HPP
#define UTILS_HPP

class utils
{
private:
    /* data */
public:
    static cv::Size resizeToFit(int width, int height, int fitWidth, int fitHeight);
    utils(/* args */);
    ~utils();
};

#endif