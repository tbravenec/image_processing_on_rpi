#include <gtkmm-3.0/gtkmm.h>
#include <opencv4/opencv2/opencv.hpp>

#ifndef APPLICATIONGUI_HPP
#define APPLICATIONGUI_HPP

class ApplicationGUI
{
private:
    Gtk::ComboBox *method;
    Gtk::ComboBox *implementation;
    Gtk::MenuItem *menuItemOpen;
    Gtk::MenuItem *menuItemSave;
    Gtk::MenuItem *menuItemClose;
    Gtk::MenuItem *menuItemStart;
    Gtk::MenuItem *menuItemClear;
    Gtk::MenuItem *menuItemAbout;
    Gtk::SpinButton *cores;
    Gtk::Image *inputImageBox;
    Gtk::Image *outputImageBox;
    Gtk::AboutDialog *aboutDialog;
    Glib::RefPtr<Gtk::Builder> builder;

    cv::Mat inputImage;
    cv::Mat outputImage;

    Glib::RefPtr<Gdk::Pixbuf> inputPixbuf;
    Glib::RefPtr<Gdk::Pixbuf> outputPixbuf;

    void on_method_changed();
    void on_implementation_changed();
    void on_gtkMenuItem_open_activate();
    void on_gtkMenuItem_save_activate();
    void on_gtkMenuItem_Start_activate();
    void on_gtkMenuItem_Clear_activate();
    void on_gtkMenuItem_About_activate();
    void on_gtkHideAbout();
public:
    Gtk::Main main;
    Gtk::Window *window;
    ApplicationGUI(/* args */);
    ~ApplicationGUI();
    void run() { main.run(*window); }
};

#endif