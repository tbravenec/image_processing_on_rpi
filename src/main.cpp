#include "ApplicationGUI.hpp"
#include "utils.hpp"

int main(int argc, char *argv[])
{
    ApplicationGUI GUI = ApplicationGUI();
    GUI.run();

    return 0;
}