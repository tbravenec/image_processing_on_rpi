#include "utils.hpp"

utils::utils(/* args */)
{
}

utils::~utils()
{
}

cv::Size utils::resizeToFit(int width, int height, int fitWidth, int fitHeight)
{
    cv::Size newSize = cv::Size(0, 0);
    float ratio;

    if(width > height) 
    {
        ratio = (float)width / (float)fitWidth;
        newSize.width = fitWidth;
        newSize.height = height / ratio;
    }
    else if(width == height)
    {
        if(fitWidth >= fitHeight)
        {
            newSize.width = fitHeight;
            newSize.height = fitHeight;
        }
        else
        {
            newSize.width = fitWidth;
            newSize.height = fitWidth;
        }
    }
    else
    {
        ratio = (float)height / (float)fitHeight;
        newSize.width = fitHeight;
        newSize.height = width / ratio;
    }

    return newSize;
}