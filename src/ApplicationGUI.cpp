#include <thread>
#include "ApplicationGUI.hpp"
#include "ImageProcessing.hpp"
#include "utils.hpp"

ApplicationGUI::ApplicationGUI(/* args */)
{
    builder = Gtk::Builder::create_from_file("GladeUI.glade");

    builder->get_widget("a_window", window);
    builder->get_widget("gtkImage_inputImage", inputImageBox);
    builder->get_widget("gtkImage_outputImage", outputImageBox);

    builder->get_widget("gtkMenuItem_open", menuItemOpen);
    menuItemOpen->signal_activate().connect(sigc::mem_fun(*this, &ApplicationGUI::on_gtkMenuItem_open_activate));

    builder->get_widget("gtkMenuItem_save", menuItemSave);
    menuItemSave->signal_activate().connect(sigc::mem_fun(*this, &ApplicationGUI::on_gtkMenuItem_save_activate));

    builder->get_widget("gtkMenuItem_close", menuItemClose);
    menuItemClose->signal_activate().connect(sigc::ptr_fun(Gtk::Main::quit));

    builder->get_widget("gtkMenuItem_Start", menuItemStart);
    menuItemStart->signal_activate().connect(sigc::mem_fun(*this, &ApplicationGUI::on_gtkMenuItem_Start_activate));

    builder->get_widget("gtkMenuItem_Clear", menuItemClear);
    menuItemClear->signal_activate().connect(sigc::mem_fun(*this, &ApplicationGUI::on_gtkMenuItem_Clear_activate));

    builder->get_widget("gtkMenuItem_About", menuItemAbout);
    menuItemAbout->signal_activate().connect(sigc::mem_fun(*this, &ApplicationGUI::on_gtkMenuItem_About_activate));

    builder->get_widget("gtkComboBox_method", method);
    method->signal_changed().connect(sigc::mem_fun(*this, &ApplicationGUI::on_method_changed));
    
    builder->get_widget("gtkComboBox_implementation", implementation);
    implementation->signal_changed().connect(sigc::mem_fun(*this, &ApplicationGUI::on_implementation_changed));
    
    builder->get_widget("aboutDialog", aboutDialog);

    builder->get_widget("gtkSpinButton_cores", cores);

    int coreCount = std::thread::hardware_concurrency();
    if(coreCount > 0)
    {
        cores->set_range(1, coreCount);
        cores->set_value(coreCount);
    }

    inputImageBox->clear();
    outputImageBox->clear();
}

ApplicationGUI::~ApplicationGUI()
{
}


// Callback methods
void ApplicationGUI::on_method_changed()
{
    Gtk::Widget *gtkBox_thresholding;
    Gtk::Widget *gtkBox_edge;
    Gtk::Widget *gtkBox_kmeans;

    builder->get_widget("gtkBox_thresholding", gtkBox_thresholding);
    builder->get_widget("gtkBox_edge", gtkBox_edge);
    builder->get_widget("gtkBox_kmeans", gtkBox_kmeans);

    if(method->get_active_row_number() == 0)
    {
        gtkBox_thresholding->show();
        gtkBox_edge->hide();
        gtkBox_kmeans->hide();
    }
    else if(method->get_active_row_number() == 1)
    {
        gtkBox_thresholding->hide();
        gtkBox_edge->show();
        gtkBox_kmeans->hide();
    }
    else if(method->get_active_row_number() == 2)
    {
        gtkBox_thresholding->hide();
        gtkBox_edge->hide();
        gtkBox_kmeans->show();
    }
}

void ApplicationGUI::on_implementation_changed()
{
    if(implementation->get_active_row_number() == 0)
    {
        cores->set_sensitive(true);
    }
    else
    {
        cores->set_sensitive(false);
    }
}

void ApplicationGUI::on_gtkMenuItem_Start_activate()
{
    Gtk::Widget *gtkBox_settings;
    Gtk::ComboBox *edgeType;
    Gtk::SpinButton *threshold_1;
    Gtk::SpinButton *threshold_2;
    Gtk::SpinButton *edgeThreshold;
    Gtk::SpinButton *clusters;
    Gtk::SpinButton *iterations;

    builder->get_widget("gtkBox_settings", gtkBox_settings);

    gtkBox_settings->set_sensitive(false);
    menuItemClear->set_sensitive(false);

    if(method->get_active_row_number() == 0)                // Thresholding
    {
        builder->get_widget("gtkSpinButton_threshold_1", threshold_1);
        builder->get_widget("gtkSpinButton_threshold_2", threshold_2);
    
        cv::cvtColor(inputImage, outputImage, cv::COLOR_RGB2GRAY);

        if(implementation->get_active_row_number() == 0)    // Custom implementation
        {
            outputImage = ImageProcessing::threshold(outputImage, threshold_1->get_value() * 255, threshold_2->get_value() * 255, cores->get_value());
            
        }
        else                                                // OpenCV
        {
            cv::threshold(outputImage, outputImage, threshold_1->get_value() * 255, threshold_2->get_value() * 255, cv::THRESH_BINARY);
        }

        cv::cvtColor(outputImage, outputImage, cv::COLOR_GRAY2RGB);
    }
    else if(method->get_active_row_number() == 1)           // Edge detection
    {
        builder->get_widget("gtkComboBox_edgeType", edgeType);
        builder->get_widget("gtkSpinButton_edgeThreshold", edgeThreshold);

        cv::cvtColor(inputImage, outputImage, cv::COLOR_RGB2GRAY);

        if(implementation->get_active_row_number() == 0)    // Custom implementation
        {
            if(edgeType->get_active_row_number() == 0)
            {
                outputImage = ImageProcessing::edgeDetection(outputImage, edgeThreshold->get_value() * 255, cores->get_value());
            }
            else
            {
                ImageProcessing::canny canny = ImageProcessing::canny(outputImage, edgeThreshold->get_value() * 255, cores->get_value());
                outputImage = canny.run();
            }
        }
        else                                                // OpenCV
        {
            if(edgeType->get_active_row_number() == 0)
            {
                cv::Mat grad_x, grad_y;
                cv::Mat abs_grad_x, abs_grad_y;

                cv::Sobel(outputImage, grad_x, CV_16S, 1, 0, 3, 1, 0, cv::BORDER_DEFAULT);
                cv::convertScaleAbs(grad_x, abs_grad_x );

                cv::Sobel(outputImage, grad_y, CV_16S, 0, 1, 3, 1, 0, cv::BORDER_DEFAULT);
                cv::convertScaleAbs(grad_y, abs_grad_y );

                cv::add(abs_grad_x, abs_grad_y, outputImage);
                cv::threshold(outputImage, outputImage, edgeThreshold->get_value() * 255, 255, cv::THRESH_TOZERO);
            }
            else
            {
                cv::Canny(outputImage, outputImage, edgeThreshold->get_value() * 255, 3 * edgeThreshold->get_value() * 255);
            }
        }

        cv::cvtColor(outputImage, outputImage, cv::COLOR_GRAY2RGB);
    }
    else if(method->get_active_row_number() == 2)           // Kmeans
    {
        builder->get_widget("gtkSpinButton_clusters", clusters);
        builder->get_widget("gtkSpinButton_maxIterations", iterations);

        if(implementation->get_active_row_number() == 0)    // Custom implementation
        {
            ImageProcessing::KMeans kmeans = ImageProcessing::KMeans(inputImage, clusters->get_value(), iterations->get_value(), cores->get_value());
            outputImage = kmeans.run();
        }
        else                                                // OpenCV
        {
            cv::Mat reshaped = inputImage.reshape(1, inputImage.rows * inputImage.cols);
            reshaped.convertTo(reshaped, CV_32F);

            std::vector<int> labels;
            cv::Mat1f colors;
            
            // Stop conditions for Kmeans - Max iterations and max difference between iterations
            cv::TermCriteria criteria = cv::TermCriteria(cv::TermCriteria::EPS + cv::TermCriteria::COUNT, (int)iterations->get_value(), 1.0); 

            cv::kmeans(reshaped, (int)clusters->get_value(), labels, criteria, 1, cv::KMEANS_RANDOM_CENTERS, colors);

            for (int i = 0; i < inputImage.rows * inputImage.cols; i++)
            {
                reshaped.at<float>(i, 0) = colors(labels[i], 0);
                reshaped.at<float>(i, 1) = colors(labels[i], 1);
                reshaped.at<float>(i, 2) = colors(labels[i], 2);
            }

            outputImage = reshaped.reshape(3, inputImage.rows);
            outputImage.convertTo(outputImage, CV_8U);
        }
    }

    try
    {
        outputPixbuf = Gdk::Pixbuf::create_from_data(outputImage.data, Gdk::COLORSPACE_RGB,false, 8, outputImage.cols, outputImage.rows, outputImage.step);
        cv::Size newSize = utils::resizeToFit(outputImage.cols, outputImage.rows, outputImageBox->get_width(), outputImageBox->get_height());
        outputPixbuf = outputPixbuf->scale_simple(newSize.width, newSize.height, Gdk::InterpType::INTERP_BILINEAR);
        outputImageBox->set(outputPixbuf);
    }
    catch(...)
    {
        /*Do nothing*/
    }
    
    gtkBox_settings->set_sensitive(true);
    menuItemClear->set_sensitive(true);

    if(outputImage.data)
    {
        menuItemSave->set_sensitive(true);
    }
}

void ApplicationGUI::on_gtkMenuItem_Clear_activate()
{
    menuItemStart->set_sensitive(false);
    menuItemClear->set_sensitive(false);
    menuItemSave->set_sensitive(false);

    inputImageBox->clear();
    outputImageBox->clear();
}

void ApplicationGUI::on_gtkMenuItem_open_activate()
{
    Gtk::FileChooserDialog dialog(*window,"Please choose an image file:", Gtk::FILE_CHOOSER_ACTION_OPEN);

    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("Select", Gtk::RESPONSE_OK);

    auto filter_img = Gtk::FileFilter::create();
    filter_img->set_name("Image files");
    filter_img->add_pattern("*.jpg");
    filter_img->add_pattern("*.png");
    filter_img->add_pattern("*.bmp");
    filter_img->add_pattern("*.tiff");
    filter_img->add_pattern("*.gif");
    dialog.add_filter(filter_img);

    int result = dialog.run();

    if(result == Gtk::RESPONSE_OK)
    {
        try
        {
            outputImageBox->clear();
            inputImage = cv::imread(dialog.get_filename());
            cv::cvtColor(inputImage, inputImage, cv::COLOR_BGR2RGB);
            inputPixbuf = Gdk::Pixbuf::create_from_data(inputImage.data, Gdk::COLORSPACE_RGB,false, 8, inputImage.cols, inputImage.rows, inputImage.step);
            cv::Size newSize = utils::resizeToFit(inputImage.cols, inputImage.rows, inputImageBox->get_width(), inputImageBox->get_height());
            inputPixbuf = inputPixbuf->scale_simple(newSize.width, newSize.height, Gdk::InterpType::INTERP_BILINEAR);
            inputImageBox->set(inputPixbuf); 
            menuItemStart->set_sensitive(true);
            menuItemClear->set_sensitive(true);
        }
        catch(...)
        {
            /* If something goes wrong with loading image, do nothing */
        }
    }
}

void ApplicationGUI::on_gtkMenuItem_save_activate()
{
    Gtk::FileChooserDialog dialog(*window,"Please choose an output image:", Gtk::FILE_CHOOSER_ACTION_SAVE);

    dialog.add_button("_Cancel", Gtk::RESPONSE_CANCEL);
    dialog.add_button("Save", Gtk::RESPONSE_OK);

    int result = dialog.run();

    if(result == Gtk::RESPONSE_OK)
    {
        cv::Mat saveImage;
        cv::cvtColor(outputImage, saveImage, cv::COLOR_RGB2BGR);
        
        cv::imwrite(dialog.get_filename(), saveImage);
    }
}

void ApplicationGUI::on_gtkMenuItem_About_activate()
{
    aboutDialog->run();
    aboutDialog->hide();
}
