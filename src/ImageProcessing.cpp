#include <omp.h>
#include "ImageProcessing.hpp"

namespace ImageProcessing
{ 
    cv::Mat threshold(cv::Mat image, unsigned char thresh, unsigned char max, int threads)
    {
        omp_set_dynamic(0);
        omp_set_num_threads(threads);

        unsigned char *data = image.data;

    #pragma omp parallel for
        for(int h = 0; h < image.rows; h++)
        {
            for(int w = 0; w < image.cols; w++)
            {
                if(data[h * image.cols + w] <= thresh)
                {
                    data[h * image.cols + w] = 0;
                }
                else
                {
                    data[h * image.cols + w] = max;
                }
            }
        }
        
        return image;
    }

    cv::Mat edgeDetection(cv::Mat image, unsigned char threshold, int threads)
    {    
        omp_set_dynamic(0);
        omp_set_num_threads(threads);

        //first index means:
        // 0: Sobel.x
        // 1: Sobel.y
        int filters[2][3][3] = { { -1,  0,  1,
                                   -2,  0,  2,
                                   -1,  0,  1 },
                                 {  1,  2,  1,
                                    0,  0,  0,
                                   -1, -2, -1 } };

        unsigned char *data = image.data;

        cv::Mat edge = cv::Mat::zeros(image.size(), CV_8U);
        cv::Mat gradX = cv::Mat::zeros(image.size(), CV_16S);
        cv::Mat gradY = cv::Mat::zeros(image.size(), CV_16S);

        unsigned char *imageData = image.data;
        unsigned char *edgeData  = edge.data;
        short *gradXdata  = (short*)gradX.data;
        short *gradYdata  = (short*)gradY.data;

    #pragma omp parallel for
        for(int h = 0; h < image.rows; h++)
        {
            for(int w = 0; w < image.cols; w++)
            {
                // nested for loops working on 2D convolution
                for (int m = 0; m < 3; m++)
                {
                    for (int n = 0; n < 3; n++)
                    {
                        if((h - 1 + m) >= 0 && (h - 1 + m) < image.rows && (w - 1 + n) >= 0 &&  (w - 1 + n) < image.cols)
                        {
                            gradXdata[h * image.cols + w] += imageData[(h - 1 + m) * image.cols + (w - 1 + n)]  * filters[0][2 - m][2 - n];
                            gradYdata[h * image.cols + w] += imageData[(h - 1 + m) * image.cols + (w - 1 + n)]  * filters[1][2 - m][2 - n];
                        }
                    }
                }

                //using faster aproximate of square root of sum of squares for saturation and thresholding
                if(abs(gradXdata[h * image.cols + w]) + abs(gradYdata[h * image.cols + w]) > 255)
                {
                    edgeData[h * image.cols + w] = 255;
                }
                else if(abs(gradXdata[h * image.cols + w]) + abs(gradYdata[h * image.cols + w]) <= threshold)
                {
                    edgeData[h * image.cols + w] = 0;
                }
                else
                {
                    edgeData[h * image.cols + w] = abs(gradXdata[h * image.cols + w]) + abs(gradYdata[h * image.cols + w]);
                }
            }
        }
        return edge;
    }

    canny::canny(cv::Mat gray, unsigned char threshold1, int threads)
    {
        image = gray;

        thresh1 = threshold1;
        
        if((short)threshold1 * 3 > 255)
        {
            thresh2 = 255;
        }
        else
        {
            thresh2 = 3 * thresh1;
        }
        
        threadCount = threads;
    }

    canny::~canny()
    {
        image.release();
        gaussian.release();
        sobel.release();
        angle.release();
        output.release();
    }

    cv::Mat canny::run()
    {
        gaussianBlur();
        sobelAndAngleMap();
        nonMaximumSuppresion();
        cannyThreshold();

        return output;
    }

    cv::Mat canny::gaussianBlur()
    {
        omp_set_dynamic(0);
        omp_set_num_threads(threadCount);

        gaussian = cv::Mat::zeros(image.size(), CV_8U);

        unsigned char *imageData = image.data;
        unsigned char *gaussianData = gaussian.data;

        double filter[5][5] = { 2,  4,  5,  4,  2,
                                4,  9, 12,  9,  4,
                                5, 12, 15, 12,  5,
                                4,  9, 12,  9,  4,
                                2,  4,  5,  4,  2 };

    #pragma omp parallel for
        for(int i = 0; i < 5; i++)
        {
            for(int j = 0; j < 5; j++)
            {
                filter[i][j] = filter[i][j] / 159;
            }
        }

    #pragma omp parallel for
        for(int h = 0; h < image.rows; h++)
        {
            for(int w = 0; w < image.cols; w++)
            {
                // nested for loops working on 2D convolution
                for (int m = 0; m < 5; m++)
                {
                    for (int n = 0; n < 5; n++)
                    {
                        if((h - 2 + m) >= 0 && (h - 2 + m) < image.rows && (w - 2 + n) >= 0 &&  (w - 2 + n) < image.cols)
                        {
                            gaussianData[h * image.cols + w] += imageData[(h - 2 + m) * image.cols + (w - 2 + n)]  * filter[4 - m][4 - n];
                        }
                    }
                }
            }
        }        

        return gaussian;        
    }

    cv::Mat canny::sobelAndAngleMap()
    {
        omp_set_dynamic(0);
        omp_set_num_threads(threadCount);

        //first index means:
        // 0: Sobel.x
        // 1: Sobel.y
        int filters[2][3][3] = { { -1,  0,  1,
                                   -2,  0,  2,
                                   -1,  0,  1 },
                                 {  1,  2,  1,
                                    0,  0,  0,
                                   -1, -2, -1 } };

        sobel = cv::Mat::zeros(gaussian.size(), CV_32F);
        angle = cv::Mat::zeros(gaussian.size(), CV_32F);

        unsigned char *gaussianData = gaussian.data;
        float *angleData = (float*)angle.data;
        float *sobelData = (float*)sobel.data;

        cv::Mat gradX = cv::Mat::zeros(gaussian.size(), CV_32F);
        cv::Mat gradY = cv::Mat::zeros(gaussian.size(), CV_32F);

        float *gradXdata  = (float*)gradX.data;
        float *gradYdata  = (float*)gradY.data;

    #pragma omp parallel for
        for(int h = 0; h < gaussian.rows; h++)
        {
            for(int w = 0; w < gaussian.cols; w++)
            {
                // nested for loops working on 2D convolution
                for (int m = 0; m < 3; m++)
                {
                    for (int n = 0; n < 3; n++)
                    {
                        if((h - 1 + m) >= 0 && (h - 1 + m) < gaussian.rows && (w - 1 + n) >= 0 &&  (w - 1 + n) < gaussian.cols)
                        {
                            gradXdata[h * gradX.cols + w] += gaussianData[(h - 1 + m) * gaussian.cols + (w - 1 + n)]  * filters[0][2 - m][2 - n];
                            gradYdata[h * gradY.cols + w] += gaussianData[(h - 1 + m) * gaussian.cols + (w - 1 + n)]  * filters[1][2 - m][2 - n];
                        }
                    }

                    // using faster aproximate of square root of sum of squares for saturation and thresholding
                    if(abs(gradXdata[h * gradX.cols + w]) + abs(gradYdata[h * gradY.cols + w]) > 255)
                    {
                        sobelData[h * sobel.cols + w] = 255;
                    }
                    else
                    {
                        sobelData[h * sobel.cols + w] = abs(gradXdata[h * gradX.cols + w]) + abs(gradYdata[h * gradY.cols + w]);
                    }

                    // Division by zero protection
                    if(gradXdata[h * gradX.cols + w] == 0)
                    {
                        angleData[h * angle.cols + w] = 90;
                    }
                    else
                    {
                        angleData[h * angle.cols + w] = atan(gradYdata[h * gradY.cols + w] / gradXdata[h * gradX.cols + w]) * 180 / M_PI;

                    }
                }
            }
        }

        return sobel;
    }

    cv::Mat canny::nonMaximumSuppresion()
    {
        omp_set_dynamic(0);
        omp_set_num_threads(threadCount);

        nonMax = cv::Mat::zeros(image.size(), CV_16U);
        unsigned short *nonMaxData = (unsigned short*)nonMax.data;
        float *angleData = (float*)angle.data;
        float *sobelData = (float*)sobel.data;

    #pragma omp parallel for
        for(int h = 1; h < angle.rows - 1; h++)
        {
            for(int w = 1; w < angle.cols - 1; w++)
            {
                // Horizontal
                if((angleData[h * angle.cols + w] > -22.5 && angleData[h * angle.cols + w] <= 22.5) || 
                   (angleData[h * angle.cols + w] > 157.5 && angleData[h * angle.cols + w] <= -157.5))
                {
                    if(sobelData[h * sobel.cols + w] == std::max(sobelData[h * sobel.cols + (w - 1)], std::max(sobelData[h * sobel.cols + w], sobelData[h * sobel.cols + (w + 1)])))
                    {
                        nonMaxData[h * nonMax.cols + w] = (unsigned short)sobelData[h * sobel.cols + w];
                    }
                }

                // Vertical
                else if((angleData[h * angle.cols + w] > -112.5 && angleData[h * angle.cols + w] <= -67.5) ||
                        (angleData[h * angle.cols + w] > 67.5 && angleData[h * angle.cols + w] <= 112.5))
                {
                    if(sobelData[h * sobel.cols + w] == std::max(sobelData[(h - 1) * sobel.cols + (w + 1)], std::max(sobelData[h * sobel.cols + w], sobelData[(h + 1) * sobel.cols + (w - 1)])))
                    {
                        nonMaxData[h * nonMax.cols + w] = (unsigned short)sobelData[h * sobel.cols + w];
                    }
                }

                // -45 degrees
                else if((angleData[h * angle.cols + w] > -67.5 && angleData[h * angle.cols + w] <= -22.5) ||
                        (angleData[h * angle.cols + w] > 112.5 && angleData[h * angle.cols + w] <= 157.5))
                {
                    if(sobelData[h * sobel.cols + w] == std::max(sobelData[(h - 1) * sobel.cols + w], std::max(sobelData[h * sobel.cols + w], sobelData[(h + 1) * sobel.cols + w])))
                    {
                        nonMaxData[h * nonMax.cols + w] = (unsigned short)sobelData[h * sobel.cols + w];
                    }
                }

                // +45 degrees
                else if((angleData[h * angle.cols + w] > -157.5 && angleData[h * angle.cols + w] <= -112.5) ||
                        (angleData[h * angle.cols + w] > 22.5 && angleData[h * angle.cols + w] <= 67.5))
                {
                    if(sobelData[h * sobel.cols + w] == std::max(sobelData[(h - 1) * sobel.cols + (w - 1)], std::max(sobelData[h * sobel.cols + w], sobelData[(h + 1) * sobel.cols + (w + 1)])))
                    {
                        nonMaxData[h * nonMax.cols + w] = (unsigned short)sobelData[h * sobel.cols + w];
                    }
                }
            }
        }

        return nonMax;
    }

    cv::Mat canny::cannyThreshold()
    {
        omp_set_dynamic(0);
        omp_set_num_threads(threadCount);

        output = cv::Mat::zeros(nonMax.size(), CV_8U);
        unsigned char *outputData = output.data;
        unsigned short *nonMaxData = (unsigned short*)nonMax.data;

    #pragma omp parallel for
        for(int h = 0; h < output.rows; h++)
        {
            for(int w = 0; w < output.cols; w++)
            {
                if(nonMaxData[h * nonMax.cols + w] >= thresh2)
                {
                    outputData[h * output.cols + w] = 255;
                }
                else if(nonMaxData[h * nonMax.cols + w] < thresh1)
                {
                    outputData[h * output.cols + w] = 0;
                }
                else
                {
                    // look for pixels around
                    for (int m = 0; m < 3; m++)
                    {
                        for (int n = 0; n < 3; n++)
                        {
                            if((h - 1 + m) >= 0 && (h - 1 + m) < output.rows && (w - 1 + n) >= 0 &&  (w - 1 + n) < output.cols)
                            {
                                if(nonMaxData[(h - 1 + m) * nonMax.cols + (w - 1 + n)] > thresh2)
                                {
                                    outputData[h * output.cols + w] = 255;
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }

        return output;
    }

    void Cluster::randomizeCentroid()
    {
        centroidBlue = rand();
        centroidGreen = rand();
        centroidRed = rand();
    }

    void Cluster::setCentroid(unsigned char blue, unsigned char green, unsigned char red)
    {
        centroidRed = red;
        centroidGreen = green;
        centroidBlue = blue;
    }
    
    void Cluster::recalculateCentroids()
    {
        if(count != 0)
        {
            centroidBlue = sumBlue / count;
            centroidRed = sumRed / count;
            centroidGreen = sumGreen / count;
        }
        clearValues();
    }
    
    void Cluster::clearValues()
    {
        sumBlue = 0;
        sumGreen = 0;
        sumRed = 0;
        count = 0;
    }

    KMeans::KMeans(cv::Mat imageData, int k, int iterations, int threads)
    {
        image = imageData;
        clustered = cv::Mat::zeros(image.size(), CV_8UC3);
        clusterCount = k;
        maxIterations = iterations;
        threadCount = threads;
        
        newCluster = (Cluster*)malloc(clusterCount * sizeof(Cluster));
        oldCluster = (Cluster*)malloc(clusterCount * sizeof(Cluster));

        for (int i = 0; i < clusterCount; i++)
        {
            newCluster[i].randomizeCentroid();
        }
    }
    
    KMeans::~KMeans()
    {
        image.release();
        free(newCluster);
        free(oldCluster);
    }

    bool KMeans::checkConvergence()
    {
        bool convergence = true;

        for (int i = 0; i < clusterCount; i++)
        {
            if (newCluster[i].centroidBlue != oldCluster[i].centroidBlue)
            {
                convergence = false;
                break;
            }
            if (newCluster[i].centroidGreen != oldCluster[i].centroidGreen)
            {
                convergence = false;
                break;
            }
            if (newCluster[i].centroidRed != oldCluster[i].centroidRed)
            {
                convergence = false;
                break;
            }
        }
        return convergence;
    }

    void KMeans::newToOld()
    {
        for (int i = 0; i < clusterCount; i++)
        {
            oldCluster[i] = newCluster[i];
        }
    }

    void KMeans::iterate()
    {
        omp_set_dynamic(0);
        omp_set_num_threads(threadCount);

        unsigned char *imageData = image.data;
        unsigned char *clusteredData = clustered.data;

    #pragma omp parallel for
        for (int i = 0; i < image.rows; i++)
        {
            for (int j = 0; j < image.cols * 3; j += 3)
            {
                int shortestDist = 0, shortestCluster = 0;
                for (int k = 0; k < clusterCount; k++)
                {
                    if (k == 0)
                    {
                        shortestDist = (pow(imageData[i * image.step + j + 0] - newCluster[k].centroidRed, 2) +
                                        pow(imageData[i * image.step + j + 1] - newCluster[k].centroidGreen, 2) +
                                        pow(imageData[i * image.step + j + 2] - newCluster[k].centroidBlue, 2));
                        shortestCluster = k;
                    }

                    if ((pow(imageData[i * image.step + j + 0] - newCluster[k].centroidRed, 2) +
                         pow(imageData[i * image.step + j + 1] - newCluster[k].centroidGreen, 2) +
                         pow(imageData[i * image.step + j + 2] - newCluster[k].centroidBlue, 2)) < shortestDist)
                    {
                        shortestDist = (pow(imageData[i * image.step + j + 0] - newCluster[k].centroidRed, 2) +
                                        pow(imageData[i * image.step + j + 1] - newCluster[k].centroidGreen, 2) +
                                        pow(imageData[i * image.step + j + 2] - newCluster[k].centroidBlue, 2));
                        shortestCluster = k;
                    }
                }

                newCluster[shortestCluster].addRedValue(imageData[i * image.step + j + 0]);
                newCluster[shortestCluster].addGreenValue(imageData[i * image.step + j + 1]);
                newCluster[shortestCluster].addBlueValue(imageData[i * image.step + j + 2]);
                newCluster[shortestCluster].increaseCount();

                clusteredData[i * image.step + j + 0] = newCluster[shortestCluster].centroidRed;
                clusteredData[i * image.step + j + 1] = newCluster[shortestCluster].centroidGreen;
                clusteredData[i * image.step + j + 2] = newCluster[shortestCluster].centroidBlue;
            }
        }
    }

    void KMeans::recalculateCentroids()
    {
        for (int i = 0; i < clusterCount; i++)
        {
            newCluster[i].recalculateCentroids();
        }
    }

    cv::Mat KMeans::run()
    {
        int numberOfIterations;

        if (maxIterations == 0)
        {
            while (true)
            {
                iterate();
                recalculateCentroids();
                if (checkConvergence() == true)
                {
                    break;
                }
                newToOld();
            }
        }
        else
        {
            for (numberOfIterations = 0; numberOfIterations < maxIterations; numberOfIterations++)
            {
                iterate();
                recalculateCentroids();
                if (checkConvergence() == true)
                {
                    break;
                }
                newToOld();
            }
        }

        return clustered;
    }
}