Image processing on Raspberry Pi
=======
This application was created as a term project for University of Technology Brno, specifically subject focused on Microcontrollers with ARM architecture. DokuWiki page of the project can be found [here][dokuwiki].

[dokuwiki]:
http://www.urel.feec.vutbr.cz/MPOA/2018/raspberry-video

## Implemented methods
All of the implemented methods allow for multithreaded computing, with the option to select how many of the systems available cores can be used. The implemented methods for image processing are:
- Tresholding
- Edge detection
  - Using simple sobel filter
  - Canny edge detector
- K-Means clustering

## Results of processing

### Thresholding
Thresholding is the most simple way of image processing. It is usuallz not used on its own, but rather as a finishing touch after other methods of image processing.

| Original Image |      Threshold 0.5     |
|:--------------:|:----------------------:|
| ![][BirdOrig]  |    ![][BirdThresh]     |

[BirdOrig]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/Bird.jpg
[BirdThresh]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/Bird_thresh.png

### Edge detection
Edge detection using sobel filter is simple and not that used, as using more complex algorithm, such as canny produces better results, with thinner edges and removed most false edges.

| Original Image |Sobel with threshold 0.9|Canny with threshold 0.15|
|:--------------:|:----------------------:|:-----------------------:|
|![][SparrowOrig]|   ![][SparrowSobel]    |    ![][SparrowCanny]    |
| ![][LennaOrig] |    ![][LennaSobel]     |     ![][LennaCanny]     |

[SparrowOrig]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/sparrow.jpg
[SparrowSobel]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/sparrow_edge_sobel_1.png
[SparrowCanny]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/sparrow_edge_canny.png
[LennaOrig]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/lenna.jpg
[LennaSobel]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/lena_sobel.png
[LennaCanny]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/lena_canny.png

### K-Means clustering
K-Means clustering for images is sometimes also caled color quantization, as the resulting image contains only as many colors as is the number of clusters.

| Original Image  |       2 Clusters       |       4 Clusters       |       8 Clusters       |
|:---------------:|:----------------------:|:----------------------:|:----------------------:|
|![][BuildingOrig]|  ![][BuildingKmeans2]  |  ![][BuildingKmeans4]  |  ![][BuildingKmeans8]  |

[BuildingOrig]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/building.jpg
[BuildingKmeans2]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/building_kmeans_2.jpg
[BuildingKmeans4]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/building_kmeans_4.jpg
[BuildingKmeans8]:
https://gitlab.com/tbravenec/image_processing_on_rpi/raw/assets/images/building_kmeans_8.jpg

## Dependencies
Even though the project is named Image processing on Raspberry Pi, it will work on any platform and any operating system as long as all the dependencies are met. These dependencies are:
- CMake
- GTK 3
- OpenCV 4
- OpenMP

On linux, the packages needed are:
- libgtkmm-3.0-dev
- libglibmm-2.4-dev
- libomp-dev 

For OpenCV 4, compilation from the source code is recommended, as version from repositories might be older version.  
Custom compilation on debian based systems can be easily done using following step by step guide:  
[How to install OpenCV 4 on Ubuntu][opencv].

  [opencv]: https://www.pyimagesearch.com/2018/08/15/how-to-install-opencv-4-on-ubuntu/

CMake was used in version 3.7, the makefile creation might work in lower version, but it is not guaranteed. Version 3.7 or higher is recommended.

## Compilation and usage
If all the dependencies are met, compilation is done by creating Makefile using cmake, and followed up by compilation itself. 
1.  cmake .
2.  make
3. ./bin/Image_Processing_on_RPi

Or combination of these
- cmake . && make && ./bin/Image_Processing_on_RPi

First two steps will then produce executable of the application. Third one will launch it. The GUI is then designed with method selection and its settings at the bottom, input and output image in the middle and controls at the top.

## License
Image processing on Raspberry Pi is released under MIT license.

## References
1. [gtkmm: C++ Interfaces for GTK+ and GNOME][gtkmm] 
2. [OpenCV: Sobel Derivatives][sobel]
3. [OpenCV: Canny Edge Detector][canny]
4. [OpenCV: Clustering][cvclustering]
5. [Data Science: K-Means Clustering][kmeans]


[gtkmm]:
https://www.gtkmm.org/en/documentation.html
[sobel]:
https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/sobel_derivatives/sobel_derivatives.html
[canny]:
https://docs.opencv.org/2.4/doc/tutorials/imgproc/imgtrans/canny_detector/canny_detector.html
[cvclustering]:
https://docs.opencv.org/2.4/modules/core/doc/clustering.html
[kmeans]:
https://www.datascience.com/blog/k-means-clustering